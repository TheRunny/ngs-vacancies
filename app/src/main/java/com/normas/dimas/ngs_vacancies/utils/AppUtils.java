package com.normas.dimas.ngs_vacancies.utils;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;

import com.normas.dimas.ngs_vacancies.presentation.inflater.Layout;

import lombok.NonNull;

public final class AppUtils {

    private AppUtils() {
        throw new UnsupportedOperationException("can't create instance of AppUtils class");
    }

    public static Intent withExtras(final Intent intent, final Bundle extras) {
        if (extras != null) {
            intent.putExtras(extras);
        }
        return intent;
    }

    @LayoutRes
    public static int getLayoutId(@NonNull final Object source) {
        final Class clazz = source.getClass();
        if (!clazz.isAnnotationPresent(Layout.class)) {
            return 0;
        }
        final Layout layoutAnnotation = (Layout) clazz.getAnnotation(Layout.class);
        return layoutAnnotation.id();
    }

}
