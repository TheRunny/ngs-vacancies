package com.normas.dimas.ngs_vacancies.presentation.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.normas.dimas.ngs_vacancies.R;
import com.normas.dimas.ngs_vacancies.presentation.base.BaseActivity;
import com.normas.dimas.ngs_vacancies.presentation.fragment.VacanciesCatalogFragment;
import com.normas.dimas.ngs_vacancies.presentation.inflater.Layout;

@Layout(id = R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showFragment(VacanciesCatalogFragment.getInstance());
    }

}
