package com.normas.dimas.ngs_vacancies.injection;


import com.normas.dimas.ngs_vacancies.App;
import com.normas.dimas.ngs_vacancies.injection.modules.AppModule;
import com.normas.dimas.ngs_vacancies.presentation.base.BaseActivity;
import com.normas.dimas.ngs_vacancies.presentation.fragment.VacanciesCatalogFragment;
import com.normas.dimas.ngs_vacancies.presentation.fragment.VacancyDetailFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    //app
    void inject(final App app);

    //activities
    void inject(final BaseActivity activity);

    //fragments
    void inject(final VacanciesCatalogFragment fragment);
    void inject(final VacancyDetailFragment fragment);

}

