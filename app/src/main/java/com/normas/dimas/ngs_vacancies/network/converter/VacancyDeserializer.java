package com.normas.dimas.ngs_vacancies.network.converter;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.normas.dimas.ngs_vacancies.network.model.response.VacancyResponse;

import java.lang.reflect.Type;

public class VacancyDeserializer implements JsonDeserializer<VacancyResponse> {

    @Override
    public VacancyResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject root = json.getAsJsonObject();
        if (root.has("vacancies")) {
            root = root.get("vacancies").getAsJsonObject();
        }

        final VacancyResponse model = new VacancyResponse();
        model.setId(root.get("id").getAsInt());
        model.setHeader(root.get("header").getAsString());

        final JsonElement company = root.get("company");
        if (!company.isJsonNull()) {
            model.setCompanyName(company.getAsJsonObject().get("title").getAsString());
            final JsonElement logo = company.getAsJsonObject().get("logo");
            if (!logo.isJsonNull()) {
                final JsonElement url = logo.getAsJsonObject().get("url");
                if (!url.isJsonNull()) {
                    model.setCompanyLogoUrl(url.getAsString());
                }
            }
        }

        model.setSalaryMin(root.get("salary_min").getAsInt());
        model.setSalaryMax(root.get("salary_max").getAsInt());
        model.setDescription(root.get("description").getAsString());

        return model;
    }
}
