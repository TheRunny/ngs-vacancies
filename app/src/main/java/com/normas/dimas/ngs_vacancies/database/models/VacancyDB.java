package com.normas.dimas.ngs_vacancies.database.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Getter;
import lombok.Setter;

@DatabaseTable(tableName = VacancyDB.TABLE_NAME)
public class VacancyDB extends BaseDB implements Parcelable {

    public static final String TABLE_NAME = "vacancies";
    public static final String COL_IMAGE = "image";
    public static final String COL_HEADER = "header";
    public static final String COL_COMPANY_NAME = "company_name";
    public static final String COL_COMPANY_LOGO_URL = "company_logo_url";
    public static final String COL_SALARY_MIN = "salary_min";
    public static final String COL_SALARY_MAX = "salary_max";
    public static final String COL_DESCRIPTION = "description";

    @Getter
    @Setter
    @DatabaseField(columnName = COL_IMAGE)
    private String image;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_HEADER, canBeNull = false)
    private String header;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_COMPANY_NAME)
    private String companyName;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_COMPANY_LOGO_URL)
    private String companyLogoUrl;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_SALARY_MIN)
    private int salaryMin;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_SALARY_MAX)
    private int salaryMax;

    @Getter
    @Setter
    @DatabaseField(columnName = COL_DESCRIPTION)
    private String description;

    public VacancyDB() {
        //empty
    }

    private VacancyDB(Parcel in) {
        image = in.readString();
        header = in.readString();
        companyName = in.readString();
        companyLogoUrl = in.readString();
        salaryMin = in.readInt();
        salaryMax = in.readInt();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(header);
        dest.writeString(companyName);
        dest.writeString(companyLogoUrl);
        dest.writeInt(salaryMin);
        dest.writeInt(salaryMax);
        dest.writeString(description);
    }

    public static final Creator<VacancyDB> CREATOR = new Creator<VacancyDB>() {
        @Override
        public VacancyDB createFromParcel(Parcel in) {
            return new VacancyDB(in);
        }

        @Override
        public VacancyDB[] newArray(int size) {
            return new VacancyDB[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

}
