package com.normas.dimas.ngs_vacancies.network;

import com.normas.dimas.ngs_vacancies.network.model.response.ArrayVacancyResponse;

import retrofit2.http.GET;
import rx.Observable;

public interface VacancyApi {

    @GET("vacancies")
    Observable<ArrayVacancyResponse> getAll();
}
