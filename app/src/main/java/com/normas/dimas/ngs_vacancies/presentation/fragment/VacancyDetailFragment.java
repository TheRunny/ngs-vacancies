package com.normas.dimas.ngs_vacancies.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.normas.dimas.ngs_vacancies.R;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;
import com.normas.dimas.ngs_vacancies.manager.VacancyManager;
import com.normas.dimas.ngs_vacancies.presentation.base.BaseFragment;
import com.normas.dimas.ngs_vacancies.presentation.inflater.Layout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Layout(id = R.layout.fragment_vacancy_detail)
public class VacancyDetailFragment extends BaseFragment {

    private static final String ARG_ID = "vacancy_id";

    @Inject VacancyManager vacancyManager;
    @BindView(R.id.header) TextView header;
    @BindView(R.id.company_title) TextView companyTitle;
    @BindView(R.id.logo) ImageView logo;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.salary) TextView salary;
    @BindView(R.id.description) TextView description;

    private int id;

    public static VacancyDetailFragment getInstance(final int vacancyId) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ID, vacancyId);
        VacancyDetailFragment fragment = new VacancyDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(ARG_ID)) {
                id = args.getInt(ARG_ID);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData(id);
    }

    private void initData(final int vacancyId) {
        final VacancyDB vacancyDB = vacancyManager.getById(vacancyId);
        header.setText(vacancyDB.getHeader());
        companyTitle.setText(vacancyDB.getCompanyName());
        final String logoUrl = vacancyDB.getCompanyLogoUrl();
        if (logoUrl != null) {
            logo.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(logoUrl)
                    .centerInside()
                    .fit()
                    .error(R.drawable.ic_warning_black_48px)
                    .into(logo, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } else {
            logo.setVisibility(View.GONE);
        }

        salary.setText(formatSalary(vacancyDB.getSalaryMin(), vacancyDB.getSalaryMax()));
        description.setText(Html.fromHtml(vacancyDB.getDescription()));
    }

    private String formatSalary(final int min, final int max) {
        String result = "";
        if (min != 0 && max == 0) {
            result = String.format(getResources().getString(R.string.salary_from), String.valueOf(min));
        } else if (min == 0 && max != 0) {
            result = String.format(getResources().getString(R.string.salary_up_to), String.valueOf(max));
        } else if (min != 0 && max != 0) {
            result = String.format(getResources().getString(R.string.salary), String.valueOf(min), String.valueOf(max));
        }
        return result;
    }
}
