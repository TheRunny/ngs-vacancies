package com.normas.dimas.ngs_vacancies.injection.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.normas.dimas.ngs_vacancies.App;
import com.normas.dimas.ngs_vacancies.BuildConfig;
import com.normas.dimas.ngs_vacancies.database.DatabaseHelper;
import com.normas.dimas.ngs_vacancies.network.VacancyApi;
import com.normas.dimas.ngs_vacancies.network.converter.VacancyDeserializer;
import com.normas.dimas.ngs_vacancies.network.model.response.VacancyResponse;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private static final String API_BASE_URL = BuildConfig.API_ROOT;

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApp() {
        return app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return app;
    }

    @Provides
    @Singleton
    public DatabaseHelper provideDBHelper(final Context context) {
        return OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    @Provides
    @Singleton
    GsonBuilder provideGsonBuilder() {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(VacancyResponse.class, new VacancyDeserializer());
        return builder;
    }

    @Provides
    @Singleton
    Gson provideGson(final GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public VacancyApi provideVacancyApi(final Gson gson) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(API_BASE_URL)
                .build()
                .create(VacancyApi.class);
    }
}
