package com.normas.dimas.ngs_vacancies.network.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;

public class ArrayVacancyResponse extends BaseResponse {

    @Getter
    @SerializedName("vacancies")
    private List<VacancyResponse> vacancies;

}
