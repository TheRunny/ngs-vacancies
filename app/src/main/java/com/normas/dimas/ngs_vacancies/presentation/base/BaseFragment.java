package com.normas.dimas.ngs_vacancies.presentation.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.normas.dimas.ngs_vacancies.injection.AppComponent;
import com.normas.dimas.ngs_vacancies.utils.AppUtils;

import butterknife.ButterKnife;
import butterknife.Optional;
import butterknife.Unbinder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

@Slf4j
public abstract class BaseFragment extends Fragment {

    private CompositeSubscription compositeSubscription;
    private Unbinder unbinder;

    protected AppComponent getAppComponent() {
        return getBaseActivity().getAppComponent();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(AppUtils.getLayoutId(this), container, false);
        compositeSubscription = new CompositeSubscription();
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    public void addFragment(final Fragment fragment) {
        final BaseActivity activity = getBaseActivity();
        if (activity != null) {
            activity.addFragment(fragment);
        }
    }

    @Optional
    protected void onBackClick() {
        final BaseActivity activity = getBaseActivity();
        if (activity != null) {
            activity.closeFragment();
        }
    }

    protected <T> Observable.Transformer<T, T> applyDefaultSchedulers() {
        return getBaseActivity().applyDefaultSchedulers();
    }

    protected synchronized void unsubscribeOnDestroy(@NonNull final Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    private BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }
}
