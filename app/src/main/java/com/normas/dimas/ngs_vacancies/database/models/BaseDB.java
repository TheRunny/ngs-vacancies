package com.normas.dimas.ngs_vacancies.database.models;

import com.j256.ormlite.field.DatabaseField;

import lombok.Getter;
import lombok.Setter;

public class BaseDB {

    public static final String ID = "id";

    @Setter
    @Getter
    @DatabaseField(columnName = ID, canBeNull = false, id = true, unique = true)
    private int id;

}
