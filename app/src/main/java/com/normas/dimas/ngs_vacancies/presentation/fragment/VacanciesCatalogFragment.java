package com.normas.dimas.ngs_vacancies.presentation.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.normas.dimas.ngs_vacancies.R;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;
import com.normas.dimas.ngs_vacancies.manager.VacancyManager;
import com.normas.dimas.ngs_vacancies.presentation.adapter.VacanciesAdapter;
import com.normas.dimas.ngs_vacancies.presentation.base.BaseFragment;
import com.normas.dimas.ngs_vacancies.presentation.inflater.Layout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.extern.slf4j.Slf4j;
import rx.Subscription;

@Slf4j
@Layout(id = R.layout.fragment_vacancies_catalog)
public class VacanciesCatalogFragment extends BaseFragment {

    @Inject VacancyManager vacancyManager;

    @BindView(R.id.vacancies_rv) RecyclerView vacanciesRv;

    public static VacanciesCatalogFragment getInstance() {
        return new VacanciesCatalogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadData() {
        Subscription subscription = vacancyManager.getAll()
                .compose(applyDefaultSchedulers())
                .subscribe(
                        this::initRecycler,
                        throwable -> log.error("Can not get vacancies." + throwable));
        unsubscribeOnDestroy(subscription);
    }

    private void initRecycler(final List<VacancyDB> items) {
        final VacanciesAdapter adapter = new VacanciesAdapter(getContext(), items);
        adapter.setOnItemClickListener(this::onClick);
        vacanciesRv.setAdapter(adapter);
        vacanciesRv.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    public void onClick(int itemId) {
        addFragment(VacancyDetailFragment.getInstance(itemId));
    }
}
