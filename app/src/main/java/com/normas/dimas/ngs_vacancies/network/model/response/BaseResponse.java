package com.normas.dimas.ngs_vacancies.network.model.response;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public class BaseResponse {

    @Getter
    @SerializedName("code")
    private int code;

}
