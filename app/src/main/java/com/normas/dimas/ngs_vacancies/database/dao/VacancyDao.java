package com.normas.dimas.ngs_vacancies.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;

import java.sql.SQLException;

public class VacancyDao extends BaseDaoImpl<VacancyDB, Integer> {
    public VacancyDao(final ConnectionSource connectionSource, final Class<VacancyDB> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
