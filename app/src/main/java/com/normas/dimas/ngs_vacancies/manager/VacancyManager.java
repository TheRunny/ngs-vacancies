package com.normas.dimas.ngs_vacancies.manager;

import com.normas.dimas.ngs_vacancies.database.DatabaseHelper;
import com.normas.dimas.ngs_vacancies.database.dao.VacancyDao;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;
import com.normas.dimas.ngs_vacancies.network.VacancyApi;
import com.normas.dimas.ngs_vacancies.network.model.response.ArrayVacancyResponse;
import com.normas.dimas.ngs_vacancies.network.model.response.VacancyResponse;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import rx.Observable;

@Singleton
@Slf4j
public class VacancyManager extends BaseApiManager {

    private final DatabaseHelper dbHelper;
    private final VacancyApi api;

    @Inject
    public VacancyManager(final DatabaseHelper dbHelper, final VacancyApi api) {
        this.dbHelper = dbHelper;
        this.api = api;
    }

    public Observable<List<VacancyDB>> getAll() {

        final VacancyDao dao;
        try {
            dao = dbHelper.getVacancyDao();
            final List<VacancyDB> list = dao.queryForAll();
            if (list != null && !list.isEmpty()) {
                return Observable.just(list);
            }
        } catch (SQLException e) {
            log.error("Can not getAll from local db." + e);
        }

        return api.getAll()
                .map(ArrayVacancyResponse::getVacancies)
                .flatMap(Observable::from)
                .map(this::createOrUpdate)
                .toList();
    }

    public VacancyDB getById(final int id) {
        try {
            final VacancyDao dao = dbHelper.getVacancyDao();
            return dao.queryForId(id);
        } catch (SQLException e) {
            log.error("Can not getById." + e);
        }
        return null;
    }

    public VacancyDB createOrUpdate(final VacancyResponse response) {
        try {
            final VacancyDao dao = dbHelper.getVacancyDao();
            final VacancyDB vacancyDB = convertToDBTask(response);
            dao.createOrUpdate(vacancyDB);
            return vacancyDB;
        } catch (SQLException e) {
            log.error(e.getLocalizedMessage(), e);
            return null;
        }
    }

    private VacancyDB convertToDBTask(final VacancyResponse response) {
        final VacancyDB vacancyDB = new VacancyDB();
        vacancyDB.setId(response.getId());
        vacancyDB.setHeader(response.getHeader());
        vacancyDB.setCompanyName(response.getCompanyName());
        vacancyDB.setCompanyLogoUrl(response.getCompanyLogoUrl());
        vacancyDB.setSalaryMin(response.getSalaryMin());
        vacancyDB.setSalaryMax(response.getSalaryMax());
        vacancyDB.setDescription(response.getDescription());
        //TODO: add fields
        return vacancyDB;
    }
}
