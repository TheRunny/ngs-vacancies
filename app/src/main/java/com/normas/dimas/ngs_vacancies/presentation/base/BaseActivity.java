package com.normas.dimas.ngs_vacancies.presentation.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.normas.dimas.ngs_vacancies.App;
import com.normas.dimas.ngs_vacancies.R;
import com.normas.dimas.ngs_vacancies.injection.AppComponent;
import com.normas.dimas.ngs_vacancies.utils.AppUtils;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@Slf4j
public abstract class BaseActivity extends AppCompatActivity {

    public AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    private CompositeSubscription compositeSubscription;

    protected synchronized void unsubscribeOnDestroy(@NonNull final Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(AppUtils.getLayoutId(this));
        getAppComponent().inject(this);
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeSubscription != null) {
            compositeSubscription.unsubscribe();
        }
    }

    public <T> Observable.Transformer<T, T> applyDefaultSchedulers() {
        return observable -> observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void onBackPressed() {
        final BaseFragment topFragment = getTopFragment();
        if (topFragment != null) {
            topFragment.onBackClick();
        } else {
            super.onBackPressed();
        }
    }

    public BaseFragment getTopFragment() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() != 0) {
            final int last = fragmentManager.getBackStackEntryCount() - 1;
            final FragmentManager.BackStackEntry entry = fragmentManager.getBackStackEntryAt(last);
            return (BaseFragment) fragmentManager.findFragmentByTag(entry.getName());
        }
        return null;
    }

    public void closeFragment() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 1) {
            finish();
        } else if (fragmentManager.getBackStackEntryCount() != 0) {
            fragmentManager.popBackStack();
        }
    }

    public void addFragment(final Fragment fragment) {
        final String tag = ((Object) fragment).getClass().getSimpleName();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public void showFragment(final Fragment fragment) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            fragmentManager.popBackStack();
        }
        final String tag = ((Object) fragment).getClass().getSimpleName();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }
}