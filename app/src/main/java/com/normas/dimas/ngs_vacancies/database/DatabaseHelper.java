package com.normas.dimas.ngs_vacancies.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.normas.dimas.ngs_vacancies.database.dao.VacancyDao;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;

import java.sql.SQLException;

import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public final class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ngs_vacancies.db";
    private static final int DATABASE_VERSION = 1;

    private static final Class[] TABLES = {
            VacancyDB.class
    };

    private VacancyDao vacancyDao;

    public DatabaseHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase database, final ConnectionSource connectionSource) {
        try {
            for (final Class table : TABLES) {
                TableUtils.createTable(connectionSource, table);
            }
        } catch (SQLException e) {
            throw new RuntimeException("error creating database", e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final ConnectionSource connectionSource,
                          final int oldVersion, final int newVersion) {
        switch (oldVersion) {
            case 1:
                // Don not use 'brake'
        }
    }

    public VacancyDao getVacancyDao() throws SQLException {
        if (vacancyDao == null) {
            vacancyDao = new VacancyDao(connectionSource, VacancyDB.class);
        }
        return vacancyDao;
    }

}
