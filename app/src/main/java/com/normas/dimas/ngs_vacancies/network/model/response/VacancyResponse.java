package com.normas.dimas.ngs_vacancies.network.model.response;

import lombok.Getter;
import lombok.Setter;

public class VacancyResponse extends BaseResponse {

    @Setter
    @Getter
    private int id;

    @Setter
    @Getter
    private String header;

    @Setter
    @Getter
    private String companyName;

    @Setter
    @Getter
    private String companyLogoUrl;

    @Setter
    @Getter
    private int salaryMin;

    @Setter
    @Getter
    private int salaryMax;

    @Setter
    @Getter
    private String description;
}
