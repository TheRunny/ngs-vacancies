package com.normas.dimas.ngs_vacancies;

import android.app.Application;

import com.normas.dimas.ngs_vacancies.injection.AppComponent;
import com.normas.dimas.ngs_vacancies.injection.DaggerAppComponent;
import com.normas.dimas.ngs_vacancies.injection.modules.AppModule;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.StatusManager;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App extends Application {

    private static final String LOGGER_PATTERN = "%d{HH:mm:ss.SSS} - %msg%n";

    @Getter
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
        configureLogger();
        log.debug("Logging system is configured.");
    }

    private void configureLogger() {
        final LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();

        final StatusManager statusManager = loggerContext.getStatusManager();
        if (statusManager != null) {
            statusManager.add(new InfoStatus("Setting up logger configuration.", loggerContext));
        }

        final Logger rootLogger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
        rootLogger.addAppender(createLogCatAppender(loggerContext));
    }

    private LogcatAppender createLogCatAppender(final LoggerContext loggerContext) {
        final PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        encoder.setPattern(LOGGER_PATTERN);
        encoder.start();

        final LogcatAppender appender = new LogcatAppender();
        appender.setContext(loggerContext);
        appender.setEncoder(encoder);
        appender.start();

        return appender;
    }
}
