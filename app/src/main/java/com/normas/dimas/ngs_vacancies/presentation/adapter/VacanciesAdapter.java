package com.normas.dimas.ngs_vacancies.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.normas.dimas.ngs_vacancies.R;
import com.normas.dimas.ngs_vacancies.database.models.VacancyDB;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static com.normas.dimas.ngs_vacancies.presentation.adapter.VacanciesAdapter.VacancyViewHolder.LAYOUT_ID;

public class VacanciesAdapter extends RecyclerView.Adapter<VacanciesAdapter.VacancyViewHolder> {

    private final Context context;
    private final LayoutInflater inflater;
    private final List<VacancyDB> data;
    private OnItemClickListener listener;

    public VacanciesAdapter(final Context context, final List<VacancyDB> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public VacanciesAdapter.VacancyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VacancyViewHolder(context, inflater.inflate(LAYOUT_ID, parent, false));
    }

    @Override
    public void onBindViewHolder(VacancyViewHolder holder, int position) {
        final VacancyDB item = data.get(position);
        holder.init(item);
        holder.layout.setOnClickListener(v -> {
            if (listener != null) {
                listener.onClick(item.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class VacancyViewHolder extends RecyclerView.ViewHolder {

        static final int LAYOUT_ID = R.layout.item_vacancy_in_catalog;

        @BindView(R.id.layout) View layout;
        @BindView(R.id.logo) ImageView logo;
        @BindView(R.id.header) TextView headerTv;
        @BindView(R.id.company_title) TextView companyTitle;
        @BindView(R.id.progress_bar) ProgressBar progressBar;

        private Context context;

        VacancyViewHolder(final Context context, final View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.context = context;
        }

        public void init(final VacancyDB item) {
            headerTv.setText(item.getHeader());
            companyTitle.setText(item.getCompanyName());
            final String logoUrl = item.getCompanyLogoUrl();
            if (logoUrl != null) {
                logo.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(logoUrl)
                        .centerInside()
                        .fit()
                        .error(R.drawable.ic_warning_black_48px)
                        .into(logo, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(GONE);
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(GONE);
                            }
                        });
            } else {
                logo.setVisibility(GONE);
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onClick(int itemId);
    }
}
